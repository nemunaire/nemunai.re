---
title: "Talks"
date: !!timestamp '2017-07-31T00:07:37+02:00'
menu:
  main:
    weight: 40
---

Here are slides I used as support to my presentations:

* [[FR] Embedding a web front-end in your Go binary: happyDomain example](/talks/happyDomain_golang_meetup_2024.pdf) [(code)](https://git.nemunai.re/nemunaire/meetup-golang-2024-demo) ([Meetup Golang Paris](https://www.meetup.com/fr-FR/golang-paris/events/301188194/))
* [[EN] Let's make people love domain names again](https://blog.happydomain.org/files/happyDomain%20FOSDEM%202024.pdf) ([FOSDEM 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-2316-let-s-make-people-love-domain-names-again/))
* [[FR] How can we make the words DNS and simplicity compatible?](https://blog.happydomain.org/files/happyDomain%20Open%20Source%20Experience%202023.pdf) ([Open Source Experience 2023](https://www.opensource-experience.com/event/#conf-13898))
* [[FR] Rethinking web platform security](/talks/lresw23-moenia.pdf) ([LRE Summer Week](https://www.lre.epita.fr/news_content/summer_week_23/))
* [[FR] Strong authentication](/talks/2fa.pdf) (*cryptoparty* GConfs)
* [[FR] Self-Hosting](/talks/autohebergement.pdf) (*cryptoparty* GConfs)
* [[FR] DNS overview](/talks/QTechNote%20DNS.pdf) (QTechNote)
* [[FR] Docker hands-on](/talks/QTechNote%20Docker.pdf) (QTechNote)
* [[FR] gRPC/Protobuf overview](/talks/QTechNote%20%231.pdf) (QTechNote)


## Teaching

In [Epita](http://www.epita.fr/), I'm responsible for teaching containers usage and plumbing in a 15 hours course called [*light virtualization*](https://virli.nemunai.re/) and for [*ADvanced LINux administration*](https://adlin.nemunai.re).

Moreover, I coach students to create [an annual CTF](https://fic.srs.epita.fr/), at [FIC].
And also lead the related CTF [server developments](https://git.nemunai.re/fic/server).

[FIC]: https://www.forum-fic.com/
