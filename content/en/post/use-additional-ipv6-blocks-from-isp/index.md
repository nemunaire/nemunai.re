---
title: Use the additional IPv6 blocks of the Free and Orange network
date: !!timestamp '2023-04-05 14:43:00'
tags:
- network
- ipv6
- freebox
---

With Free and Orange, when IPv6 is not disabled, the Freebox (and some Livebox) provide a /64 IPv6 range to the connected equipment.
But it turns out that it is a /60 range that is available and usable by each subscriber.
This represents a total of 8 addressable /64 networks.
Let's see what it can be used for and how to use it.

<!-- more -->

# IPv6 reminders

Contrary to IPv4, with IPv6 one avoids making NAT, i.e. one allocates to each machine on the network an IPv6 address directly routable on Internet.
Of course it is always necessary to go through the router (the box) which is then used as a simple gateway to the Internet.

In IPv6, devices are able to choose their own IP, without the help of the DHCP protocol.
This is possible because the router regularly transmits information about the subnet in which you are located (this is the [Router Advertisement (RA)](https://en.wikipedia.org/wiki/Router_advertisement)).

For our experiment, let's take the following lab:

![The basic infrastructure that we will use for our experiments](lab.png)

We have all our equipment connected to the box and a series of virtual machines hosted on one of the network machines.

At this stage, if we want our virtual machines to be reachable from the Internet in IPv6, we have to configure the hypervisor network in *bridge* mode.\
Indeed if the network of our virtual machines is distinct from the network of the box, this one will not be able to communicate with our virtual machines. By using the *bridge* mode, we simulate the fact that the virtual machine is diconnected to the box, or to a switch. In any case no equipment requiring to make routing.

If our virtual machines are only IPv6 clients and are not intended to serve content directly on the Internet, this solution is perfectly acceptable. But if we want to serve content, we might want to segment our network to try to isolate the content. But if we want to serve content, we might want to segment our network to try to isolate the devices.


# Segment the network of the box

Because of the very large number of public IPv6 addresses that our operators provide us with, we could start by segmenting our network between our virtual servers and our other equipment: each would be in a separate subnet.

The main interest of this segmentation would be to avoid that all this little world shares the same subnet: as they can all communicate directly with each other, it is more complicated to filter efficiently malicious exchanges. For example, if one of the virtual machines exposed on the Internet is compromised, it can access all our local equipment (telephones, connected objects, etc.) which are not necessarily secure, or conversely, an object on the network can start to intercept all the data. network object can start intercepting all the traffic of the virtual machines by pretending to be the box.

We could therefore want to segment our network like this:

![Example of segmentation by splitting the /64 block into two /65 blocks](lab-segmente.png)

We would reserve half of the /64 block for real network equipment and allocate the other half to our virtual machines located on a server/Raspberry Pi.

Despite the large number of IPv6 addresses that can be assigned, it is not easy to subdivide our /64 to assign it to a secondary router or a virtual machine server. This segmentation is indeed not possible without changing the configuration of the box because it expects to be able to reach our virtual machines directly, without going through an intermediate machine/host/router.

However, we have access to the routing parameters of the other 7 /64 blocks distributed by the operator. We can for example assign one of them to the host of our virtual machines.


# Delegate an additional IPv6 prefix

As mentioned in the introduction, some operators make available to their subscribers a range of IPv6 addresses much larger than the /64 block of the main network.

Some ISP routers also make it possible to take advantage of additional blocks by offering to delegate the other blocks to machines on the network.

In concrete terms, this means that when the box receives a packet destined for one of the delegated blocks, it will not process it itself, but will transmit it to the machine designated as the recipient. In other words, it will route the traffic of this block to the designated router. And it doesn't have to be complicated!


# Different use cases

Now that we have seen the theory, let's look at different use cases, so we are not limited to our virtual machines:

- Use a /64 block to give IPv6 to its virtual machines
- [Use a /64 block to give IPv6 to your Docker containers]({{< relref "use-ipv6-in-docker.md" >}})
- Use a /64 block to have IPv6 in several isolated subnets
- Use a /64 block to have public IPv6 in your Wireguard tunnel
