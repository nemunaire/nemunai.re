---
title: My private SSH keys managment
date: !!timestamp '2015-02-19 00:00:00'
update: !!timestamp '2017-07-23 22:45:00'
tags:
    - cryptography
    - ssh
---

I always have a different SSH key pair per machine. The aim is to really never copy my private key from a machine to another over network or USB stick.

<!--more-->

## Client keys

With this approach, if one of my host is compromised and/or my key could have been exposed, I have only to remove granted access to this key to host or services (OK, that can be painful to find such services), but I can continue to use other no-compromised keys to work.

As you can see on my [github](https://github.com/nemunaire.keys) account, I've registered several keys, because I don't work from the same machine every time.

It can sometime be complicated to give me access to machine, but in most case, I tend to centralize most of my outgoing connections from a single host, which is in fact my home desktop: oupaout.

Here is a list of my keys' md5 fingerprints:

* `assouan`
* [`aton`](aton_ed25519.pub): 0d:89:02:4a:45:51:0c:43:e8:be:2e:99:38:5b:88:0e (ED25519)
* [`aton`](aton_rsa.pub): ee:61:d5:bf:b0:23:93:1a:bb:32:ef:34:10:fb:aa:77 (RSA 4096)
* `bastet` (legacy): 4a:51:80:24:b0:69:7a:59:fc:44:08:29:aa:15:42:5b (RSA 4096)
* EPITA personal rack (legacy): 91:95:bc:4e:e7:b2:5b:9c:7f:71:4a:7d:0a:43:80:17 (RSA 4096)
* EPITA SSH gate (legacy)
* EPITA YACKU laboratory (legacy): 80:7c:8e:42:53:ee:0f:b5:27:d5:63:ab:b4:5a:46 (RSA 4096)
* [`khonsou`](khonsou_ed25519.pub): 0d:89:02:4a:45:51:0c:43:e8:be:2e:99:38:5b:88:0e (ED25519)
* khonsou_old: f5:dc:fd:db:c8:ce:ec:df:33:86:54:58:05:7e:d2:74 (ED25519)
* `montou`: this machine can't be used as outgoing host.
* `nout`: this machine can't be used as outgoing host.
* `nout_old`: e8:69:71:3c:5e:cc:3c:d5:7d:a2:67:30:a9:35:df:24 (RSA 4096)
* [`ouaset`](ouaset.pub): 5e:01:65:8c:ae:9e:6e:f3:a5:88:80:16:fa:bf:d6:ac (ED25519)
* [`oupaout`](oupaout.pub): 30:04:ad:11:57:e0:e9:dc:a2:e0:d2:65:cd:60:9a:ab (ED25519)
* [Rescue key](rescue.pub): 30:15:a7:3f:0b:51:7a:53:7b:47:bd:00:21:55:ee:bd (RSA 4096)
* `satis`: this machine has no key pair yet.
* [`seth`](seth_ed25519.pub): 6a:1f:05:60:fa:6b:32:f3:2d:ba:e3:36:e7:6b:7b:13 (ED25519)
* [`seth`](seth_rsa.pub): 03:a9:3a:3a:e1:e0:99:24:69:15:cb:a5:58:5c:3f:6c (RSA 4096)
* `seth_old`: 97:8c:bc:9a:ec:62:8d:b8:1a:88:b1:0d:d9:62:1a:04 (RSA 4096)

### Get the fingerprint from a private or public key file

```sh
ssh-keygen -l -E md5 -f KEY_FILE
```


### Usign PGP

Sometime, I use my authentication PGP key as SSH key. Read the [related article]({{< relref "/post/pgp_key.md#ssh-authentication" >}}) to view the public key.


## Server keys

The `nemunai.re` domain, contains [SSHFP] records for each physical host. To avoid answering this message without further checks:

    The authenticity of host 'nemunai.re (203.0.113.42)' can't be established.
    RSA key fingerprint is 00:11:22:33:44:55:66:77:88:99:aa:bb:cc:dd:ee:ff.
    Are you sure you want to continue connecting (yes/no)?

you can use the following command: `ssh -o "VerifyHostKeyDNS yes" $HOSTNAME.nemunai.re`

[SSHFP]: http://tools.ietf.org/html/rfc4255


### Generate SSHFP records

SSHFP records can be generated with the following command:

```sh
ssh-keygen -r HOSTNAME
```
