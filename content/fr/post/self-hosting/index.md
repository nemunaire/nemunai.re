---
title: "L'auto-hébergement : pour un Internet décentralisé et responsable"
date: !!timestamp '2023-05-22 10:05:00'
image: /post/self-hosting/og.webp
tags:
- privacy
- hosting
---

Dans un monde idéalisé, nous serions tous libres d'accéder à tout service, mis à disposition par une communauté motivée par le bien commun plutôt que par l'enrichissement pénunié d'un petit nombre.
Cependant, nous ne vivons pas dans ce monde.

Aujourd'hui, tout se monnaye : le moindre produit ou service rendu n'existe que s'il rapporte suffisamment d'argent, sans considération de l'intérêt commun.
La gratuité apparente des services numériques s'accompagne souvent d'une contrepartie plus discrète mais pourtant bien lucrative : l'exploitation de nos données personnelles.

L'auto-hébergement, ou l'idée d'héberger soi-même ses propres services, apparaît comme une pratique d'hygiène numérique obligatoire.

<!-- more -->

*Cet article est une retranscription de la conférence que j'ai donnée à l'EPITA sur l'auto-hébergement : [voir les slides](/talks/autohebergement.pdf)*

## L'influence des régies publicitaires 🎭

Les plus grandes régies publicitaires du monde ont développé des stratégies astucieuses pour influencer notre comportement. Mais au fait, les connaissez-vous ?

Les deux plus connues sont Facebook et Google. La seconde a relevé le défi d'indexer le web en nous proposant des publicités en lien avec nos recherches. Jusque là, c'est très classique et pas si alarmant que ça.
D'ailleurs, pendant de longues années, on aurait presque pu penser que pour une régie publicitaire, le service « annexe » de recherche était loin d'être bancal.

Mais un jour, un ingénieur publicitaire a eu une idée : si on pouvait savoir quel genre d'individu fait une recherche, on serait capable de l'envoyer directement vers le site le plus approprié.
Quelques développements plus tard, voici ce que fût le plus grand aspirateur de données que l'on puisse imaginer :

![L'interface de GMail](gmail.png)

Contacts, contenus des messages, bons de commandes, newsletters, ... Voilà comment orienter subtilement quelqu'un vers un site plutôt qu'un autre.
C'est très bien lorsqu'on est pressé, mais au lieu de nous ouvrir sur le monde en général, cela nous enferme au contraire dans une bulle informative.

Au delà des œillères que les puissants algorithmes nous mettent, ils sont aussi capables de mettre en avant des articles promotionnels : si un jour vous avez eu l'intention d'acheter du produit X, soyez sûr qu'il sera mis en avant si cela peut rapporter de l'argent à l'annonceur. C'est là que la ligne est franchie.


## Quel lien avec l'auto-hébergement ? 🎯

De nombreuses entreprises (et même des associations) ont commencées avec des ambitions louables.
Puis bien souvent, appâtées par le gain des données personneeles, le rachat par un concurrent, ... les ambitions s'estompent.
Mais les dirigeants savent bien jauger ces transformations afin de ne pas provoquer un départ massif de leurs clients :

[Saviez-vous par exemple que les adresses @orange.fr ou @wanadoo.fr expirent 6 mois après la résiliation de l’abonnement ?](https://assistance.orange.fr/assistance-commerciale/la-gestion-de-vos-offres-et-options/resilier-votre-offre/les-conseils-avant-de-resilier-votre-messagerie-mail-orange_71178-72015)

On peut se demander pourquoi les FAI fournissent gratuitement un service d'hébergement de courriers électroniques, c'est louable au premier abord, mais c'est aussi un solide argument qui, lorsqu'on est habitué à utiliser ce service, nous rend dépendant les évolutions du service et de l'offre d'abonnement Internet qui en dépend. Jusqu'à quelle augmentation soudaine serions-nous prêt à fermer les yeux pour ne pas avoir à demander à tous nos contacts de changer notre adresse dans leur carnet, réimprimer nos cartes de visites, ou refaire le flocage de notre utilitaire professionel (sur lequel est fièrement écrit plombier-valclair@orange.fr), ... ?

Le même problème peut être extrapolé aux services de Google : avec les régulations de plus en plus contraignantes sur les données personnelles, peut-être qu'un jour GMail finira par être payant, ou peut-être sera-t-il coupé brutalement car pas assez rentable[^KILLEDBYGOOGLE].

[^KILLEDBYGOOGLE]: Comme tous ces services : <https://killedbygoogle.com/>

Et bien sûr tous les prestataires peuvent être concernés... Alors, que faire ?


## Auto-hébergement niveau 0 : Avoir son propre nom de domaine 🔑

Il est essentiel de posséder son propre nom de domaine pour assurer l'interopérabilité et la continuité en cas de changement de fournisseur de services.

Pour une dizaine d'euros par an, disposer d'un nom de domaine vous permet d'être libre de changer quand bon vous semble de prestataires, moyennant des contraintes techniques dont vous êtes maître.
C'est-à-dire que dans le cas d'un changement de prestataire de courriers électroniques, vous n'aurez plus besoin de prévenir tous vos contacts et de gérer pendant des mois les irréductibles que vous aurez oublié de contactés ou qui auront oubliés de mettre à jour leur carnet d'adresse (ou qui, dans le doute, mettront systématiquement toutes vos adresses...).

La gestion d'un nom de domaine est à la portée de tous, vous pouvez d'ailleurs utiliser [happyDomain](https://happydomain.org/) pour vous aider dans cette tâche.


## Auto-hébergement niveau 1 : Utiliser des services responsables et éthiques 🌿

Alors que la dépendance vis-à-vis des grandes entreprises technologiques peut sembler inévitable, il existe des alternatives plus respectueuses de l'utilisateur.
Par exemple, des fournisseurs de messagerie comme ProtonMail proposent des services sécurisés et respectueux de la vie privée pour un coût modique.

De nombreux hébergeurs proposent un pack lorsqu'on achète un nom de domaine : c'est à dire que pour 10€/an, il est possible d'avoir une adresse électronique sans se prendre la tête.
Généralement les adresses supplémentaires sont payantes.


## Auto-hébergement niveau 2 : En datacenter 🏭

Les solutions précédentes sont un premier pas, on ne peut pas parler d'auto-hébergement, puisque l'on dépend encore de prestataires de services.
Néanmoins, si vous n'avez pas le temps et l'énergie à consacrer à vous auto-héberger, ces sont des solutions tout-à-fait viables.
Ensuite, l'autohébergement en datacenter est une étape vers l'indépendance totale.

En choisissant d'héberger tout ou partie de vos services sur votre/vos propre serveur, vous vous libérez de la dépendance vis-à-vis des fournisseurs de services, et de leurs politiques changeantes.

Cela demande un certain niveau de connaissances techniques, mais avec de l'entrainement et de la volonté, c'est tout-à-fait réalisable.

Les coûts d'hébergement en datacenter sont relativement faibles, avec des offres comme Kimsufi d'OVH et Dédibox de Scaleway proposant des serveurs dédiés à partir de 5€ à 10€ par mois.
On peut même trouver moins cher en partant sur des VPS : ce sont des serveurs virtuellement partagés entre plusieurs utilisateurs. C'est idéal pour réduire les coûts et se jeter à l'eau.


## Quels logiciels choisir ? 📚

L'autohébergement nécessite des logiciels appropriés pour la configuration et la gestion de votre serveur.
Des distributions comme YunoHost fournissent une suite complète de services auto-hébergés avec une interface web pour l'administration, rendant le processus plus accessible aux non-initiés.


## Auto-hébergement niveau 3 : À la maison 🏡

L'autohébergement à la maison est l'ultime étape vers l'indépendance numérique.
Des petits « serveurs » comme les Raspberry Pi, Pine64, ... permettent de se configurer un serveur personnel pour un coût modique.

Attention cependant, l'hébergement à domicile peut s'avérer contraignant : tous les opérateurs ne permettent pas ou pas facilement, de s'auto-héberger.
Il faut se renseigner et faire quelques expérimentations.


## Auto-hébergement niveau 4 : Chez un fournisseur d'accès associatif 🤝

Enfin, le dernier niveau d'auto-hébergement consiste à souscrire une offre d'accès à Internet chez un fournisseur associatif, en Frande, un membre de la [FFDN](https://www.ffdn.org/).

---

L'auto-hébergement est une solution pour ceux qui souhaitent prendre le contrôle de leurs données et protéger leur vie privée.
Que vous choisissiez de vous auto-héberger à la maison ou de faire confiance à des fournisseurs de services éthiques, vous contribuerez à rendre Internet un peu plus respectueux des utilisateurs.
Ce parcours ne sera peut-être pas simple, mais il sera gratifiant.
Et n'oubliez pas, vous ne serez pas seul.
Il existe de nombreuses ressources et communautés prêtes à vous aider dans cette aventure.

Alors, êtes-vous prêt à reprendre le contrôle ?

N'oubliez pas que l'auto-hébergement est un voyage, pas une destination. Chaque étape que vous franchissez renforce votre indépendance numérique. C'est un défi, certes, mais c'est aussi une façon de renforcer vos compétences et de vous rapprocher de la véritable essence d'Internet - une plateforme ouverte et décentralisée, où tout le monde a le pouvoir de créer et de partager.

Il est temps de renverser la tendance. L'auto-hébergement n'est pas seulement une manière de reprendre le contrôle sur vos données, mais aussi une contribution à un Internet plus décentralisé et équitable. N'attendez plus, embarquez dans l'aventure ! 🚀
