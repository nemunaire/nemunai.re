---
title: PGP key
date: !!timestamp '2015-06-29 00:00:00'
update: !!timestamp '2021-07-24 00:45:00'
tags:
    - privacy
    - cryptography
---

My personal PGP key is the following: [0x842807a84573cc96].

    pub   4096R/4573CC96 2014-06-23 [expires: 2022-06-30]
          Key fingerprint = E722 B5B7 3CA7 FA93 5FC1  AA09 8428 07A8 4573 CC96
    uid                  Pierre-Olivier Mercier <nemunaire@nemunai.re>
    sub   4096R/9D2855C3 2014-06-23 [expires: 2022-06-30]

<!--more-->

I use PGP on a daily basis: each e-mail I sent is at least signed. Don't hesitate to send me encrypted or signed message.

My keyring is stored on a tamper resistant USB token (a [Nitrokey Pro]).
This is the only method I use to sign, encrypt and sometimes to [authenticate](#ssh-authentication).


## DANE

My key is also available through [OpenPGP DANE].
You can retrieve it using `gpg` via:

```sh
gpg2 --auto-key-locate clear,dane -v --locate-key nemunaire@nemunai.re
```

I used [this script](https://gist.github.com/nemunaire/447c989e9f098c679edb) to generate the record.
With modern version of `gnupg`, it is also possible to get the DNS entry with the following command:

```sh
gpg2 --export-options export-minimal,export-dane --export 0xKEYID
```


## SSH Authentication

Sometimes I use my dedicated PGP key to log me on a remote SSH server. Here is its corresponding public ssh key :

    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCnABr9AhXL9AYBQnM0GRcR9yLaKheLcxXykTSEbKP4X/R5BElSS5iF+T+MPi1ym7AtcuFcHXqNdEhb3j6zvqk3sY069sg0zio/jQzWTtKjYVtCiE4SleFrb5I012IwFhPCUArVqhHrfuj8wtg5isl1CSIYii+bpFLbrAGqBcydfcN/Z/vd7jbGEdmHR1RYwE+TzJl1aPiWpqMg9PyaJudNcuxrWjHcHtAomT0CGn2OGREUZS9rcFomCqw7JW9moaWqDSaW+aNX+xTJISo6TiAB4nNOpvTMl6BWPJ5e2eqn4xQACuTb/EVCuAJGeQ8BQudanxRXrfpdgHATsJxldTau2CCmIrZrg2We0ZfiGZ7KEwf3isAyzH9FK/gmb29XeDfvE/UpTTijaPo8xgiH3Ag0ZBk1wb3PVneN9fQGpVogOxR/HwfqOl376N6kTQIhvAFaU/wJnHQ4Z0CBekOxC9XptrihUdW7ashP6arrhYzlyNUPrRGiLmab9jsqsvP7aDRFEpWa/cd9nD2Mp1JNj51ZeqwT5Juo3ElMCfoTy5IAyc6QUTtIdYRgukLjiO8k5NBi8/Yzm1lNzf3cRZdh5ZIS0AUO2Celi97WXiHrU841OqsuMBgdCDnOuG3X8qU+pyT7836XSjglLEwABtXUSWULf06AoPVJe4+cxi3NWfxJiQ==


## Teaching PGP

Each year, I ask my students at [EPITA](https://www.epita.fr/), a French computer science school, to sign their work when they send them to me, by e-mail.

As it is not always easy for them, I developed a script to automatically check the correctness of their signature: [peret](https://git.nemunai.re/srs/peret).


[0x842807a84573cc96]: http://pgp.mit.edu/pks/lookup?op=vindex&search=0x842807A84573CC96
[Nitrokey Pro]: https://shop.nitrokey.com/shop/product/nitrokey-pro-3
[OpenPGP DANE]: https://tools.ietf.org/html/rfc7929
