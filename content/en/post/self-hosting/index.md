---
title: "Self-hosting: for a decentralized and responsible Internet"
date: !!timestamp '2023-05-22 10:05:00'
image: /post/self-hosting/og.webp
tags:
- privacy
- hosting
---

In an idealized world, we would all be free to access any service, made available by a community motivated by the common good rather than by the penurious enrichment of a few.
However, we don't live in that world.

Today, everything is monetized: the smallest product or service only exists if it brings in enough money, without regard for the common good.
The apparent gratuity of digital services is often accompanied by a more discreet but lucrative counterpart: the exploitation of our personal data.

Self-hosting, or the idea of hosting one's own services, appears to be a mandatory digital hygiene practice.

<!-- more -->

*This article is a transcript of the conference I gave at EPITA on self-hosting: [see the slides (in french)](/talks/autohebergement.pdf)*

## The influence of advertising agencies 🎭

The world's largest advertising companies have developed clever strategies to influence our behavior. But do you know them?

The two best known are Facebook and Google. The latter has taken up the challenge of indexing the web by offering us ads related to our searches. So far, it's very classical and not so alarming.
In fact, for many years, one could almost think that for an advertising company, the "annex" service of search was far from being flawed.

But one day, an advertising engineer had an idea: if we could know what kind of person is searching, we would be able to send them directly to the most appropriate site.
A few developments later, here is what the biggest data vacuum cleaner you can imagine was:

![The GMail interface](gmail.png)

Contacts, message contents, order forms, newsletters, ... This is how to subtly direct someone to one site rather than another.
This is very good when we are in a hurry, but instead of opening us up to the world in general, it locks us into an information bubble.

Beyond the blinkers that powerful algorithms put on us, they are also able to put forward promotional items: if one day you intended to buy product X, be sure that it will be put forward if it can make money for the advertiser. This is where the line is crossed.


## How does this relate to self-hosting? 🎯

Many companies (and even associations) started with laudable ambitions.
Then very often, baited by the gain of personal data, the takeover by a competitor, ... ambitions fade away.
But managers know how to gauge these transformations in order not to provoke a massive departure of their customers:

[Did you know for example that @orange.fr or @wanadoo.fr addresses expire 6 months after the subscription is cancelled?](https://assistance.orange.fr/assistance-commerciale/la-gestion-de-vos-offres-et-options/resilier-votre-offre/les-conseils-avant-de-resilier-votre-messagerie-mail-orange_71178-72015)

One may wonder why ISPs provide a free e-mail hosting service, it is commendable at first glance, but it is also a strong argument that, when we are used to using this service, makes us dependent on the evolutions of the service and the Internet subscription offer that depends on it. Until what sudden increase would we be ready to close our eyes in order not to have to ask all our contacts to change our address in their book, to reprint our business cards, or to redo the flocking of our professional utility (on which is proudly written plombier-valclair@orange.fr), ... ?

The same problem can be extrapolated to Google's services: with the more and more restrictive regulations on personal data, maybe one day GMail will end up being paid, or maybe it will be abruptly cut because not profitable enough[^KILLEDBYGOOGLE].

[^KILLEDBYGOOGLE]: Like all these services: <https://killedbygoogle.com/>

And of course all providers can be concerned... So, what to do?


## Self-hosting level 0: Having your own domain name 🔑

Having your own domain name is essential to ensure interoperability and continuity when changing service providers.

For about ten dollars per year, having a domain name allows you to be free to change providers whenever you want, subject to technical constraints that you control.
That is to say that in the case of a change of e-mail provider, you will no longer need to warn all your contacts and to manage for months the diehards that you will have forgotten to contact or who will have forgotten to update their address book (or who, in doubt, will systematically put all your addresses...).

The management of a domain name is within everyone's reach, you can use [happyDomain](https://happydomain.org/) to help you in this task.


## Self-hosting level 1: Using responsible and ethical services 🌿

While dependence on large technology companies may seem inevitable, there are more user-friendly alternatives.
For example, email providers like ProtonMail offer secure, privacy-friendly services for a small fee.

Many hosting companies offer a package when you buy a domain name: that is to say that for 10$/year, it is possible to have an e-mail address without having to worry about it.
Generally, additional addresses are not free.


## Self-hosting level 2 : In datacenter 🏭

The previous solutions are a first step, we can't talk about self-hosting, since we still depend on service providers.
Nevertheless, if you don't have the time and energy to devote to self-hosting, these are quite viable solutions.
Secondly, self-hosting in a datacenter is a step towards total independence.

By choosing to host all or part of your services on your own server(s), you free yourself from dependence on service providers and their changing policies.

This requires a certain level of technical knowledge, but with practice and willingness, it is quite feasible.

Datacenter hosting costs are relatively low, with offers like OVH's Kimsufi and Scaleway's Dedibox offering dedicated servers from 5€ to 10€ per month.
You can even find cheaper VPS: these are servers that are virtually shared between several users. It's ideal to reduce costs and get into the water.

## Which software to choose? 📚

Self-hosting requires appropriate software to configure and manage your server.
Distributions like YunoHost provide a full suite of self-hosted services with a web interface for administration, making the process more accessible to the uninitiated.


## Self-catering level 3: At home 🏡

Self-hosting at home is the ultimate step towards digital independence.
Small "servers" like the Raspberry Pi, Pine64, ... allow to set up a personal server for a modest cost.

Be careful though, hosting at home can be restrictive: not all operators allow you to host yourself, or not easily.
It is necessary to inquire and to make some experiments.


## Self-hosted level 4 : At an associative access provider 🤝

Finally, the last level of self-hosting consists in subscribing to an Internet access offer from an association provider.

---

Self-hosting is a solution for those who want to take control of their data and protect their privacy.
Whether you choose to self-host at home or trust ethical service providers, you'll help make the Internet a little more user-friendly.
It may not be an easy journey, but it will be a rewarding one.
And remember, you won't be alone.
There are many resources and communities ready to help you on this journey.

So, are you ready to take back control?

Remember, self-hosting is a journey, not a destination. Every step you take strengthens your digital independence. It's a challenge, sure, but it's also a way to strengthen your skills and get closer to the true essence of the Internet - an open, decentralized platform where everyone has the power to create and share.

It's time to turn the tide. Self-hosting is not only a way to regain control over your data, but also a contribution to a more decentralized and equitable Internet. Don't wait any longer, get on board! 🚀
