---
title: "Conférences"
date: !!timestamp '2017-07-31T00:07:37+02:00'
menu:
  main:
    weight: 40
aliases:
  - talks
---

Voici les supports des conférences que j'ai données :

* [[FR] Embarquer un frontal web dans son binaire Go, exemple avec happyDomain](/talks/happyDomain_golang_meetup_2024.pdf) [(code)](https://git.nemunai.re/nemunaire/meetup-golang-2024-demo) ([Meetup Golang Paris](https://www.meetup.com/fr-FR/golang-paris/events/301188194/))
* [[EN] Let's make people love domain names again](https://blog.happydomain.org/files/happyDomain%20FOSDEM%202024.pdf) ([FOSDEM 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-2316-let-s-make-people-love-domain-names-again/))
* [[FR] Comment rendre les mots DNS et simplicité compatibles ?](https://blog.happydomain.org/files/happyDomain%20Open%20Source%20Experience%202023.pdf) ([Open Source Experience 2023](https://www.opensource-experience.com/event/#conf-13898))
* [[FR] Repenser la sécurité des plateformes web](/talks/lresw23-moenia.pdf) ([LRE Summer Week](https://www.lre.epita.fr/news_content/summer_week_23/))
* [[FR] L'authentification forte](/talks/2fa.pdf) (*cryptoparty* GConfs)
* [[FR] L'autohébergement](/talks/autohebergement.pdf) (*cryptoparty* GConfs)
* [[FR] Le DNS](/talks/QTechNote%20DNS.pdf) (QTechNote)
* [[FR] Prise en main de Docker](/talks/QTechNote%20Docker.pdf) (QTechNote)
* [[FR] Prise en main de gRPC/Protobuf](/talks/QTechNote%20%231.pdf) (QTechNote)


## Enseignement

À l'[Epita](http://www.epita.fr/), j'ai enseigné durant 10 ans l'usage des conteneurs et leur fonctionnement technique au sein du noyau Linux dans un cours de 24 heures nommé [*Virtualisation légère*](https://virli.nemunai.re/) ainsi que l'[*ADministration LINux avancée*](https://adlin.nemunai.re).

D'autre part, j'encadre le projet de fin d'études des étudiants de la majeure SRS pour lequel ils conçoivent un [challenge de forensic](https://fic.srs.epita.fr/) pour l'[European Cyber Cup](https://european-cybercup.com), présenté au [Forum International de la Cybersécurité](https://www.forum-fic.com/).
À ce titre, je maintiens et coordonne les développements de [la plateforme de validation](https://git.nemunai.re/fic/server).
