---
title: "Maîtrisez votre IA : explorez l'IA locale grâce à OVH et 1 €"
date: !!timestamp '2024-09-04 11:12:00'
image: /post/evaluate-local-ai-with-ease-on-ovhcloud/og.webp
toc: true
tags:
- ai
- cloud
- hosting
- privacy
---

Les IA génératives répondent aujourd’hui à de nombreux usages, et il serait dommage de ne pas en tirer parti.
Cependant, à l’heure du RGPD, il est légitime de se questionner sur l’exposition des données de ses clients ou partenaires lorsqu’on se repose uniquement sur des prestataires d'IA dont le traitement des données reste opaque.

Ayant moi-même participé à la construction de l’un de ces nouveaux services d’assistant virtuel, je vous propose ici un guide simple pour démarrer une machine et évaluer les performances de votre application IA, ou [pour offrir à vos employés un accès à une IA souveraine](https://nextcloud.com/blog/nextcloud-releases-assistant-2-0-and-pushes-ai-as-a-service/), dans le but de comparer facilement les différents modèles disponibles.

<!-- more -->

Ce tutoriel vous expliquera comment migrer votre application existante d’un prestataire externe vers votre propre service d’IA dans le cloud.

## Prérequis

Pour mener à bien ce test, nous allons avoir besoin :

- d'un compte OVHcloud,
- d'un nom de domaine (hébergé par OVH ou non),
- d'un sous-domaine dédié à cette expérimentation,
- d'une application à migrer,
- 1€.

### Nom de domaine

Dans la suite de ce tutoriel, je vais utiliser le domaine `example.com`.
Au sein de ce domaine, j'ai réservé le sous-domaine `ia` à l'expérience.
Lorsque vous verrez `ia.example.com.`, n'hésitez pas à le remplacer par votre propre domaine.


### Notre application exemple

Voici un exemple très basique d'application utilisant ChatGPT d'OpenAI :

```python
import os
from openai import OpenAI

client = OpenAI(
    api_key=os.environ.get("OPENAI_API_KEY"),
)

chat_completion = client.chat.completions.create(
    messages=[
        {
            "role": "user",
            "content": "Propose-moi un itinéraire de voyage pour visiter la Norvège",
        }
    ],
    model="gpt-4",
)

print(chat_completion.choices[0].message.content)
```

Il s'agit d'un banal script posant une question à un assistant virtuel et affichant sa réponse en retour, sur la sortie standard.

Dans le cas d'une application plus complexe, nous verrons que les changements à apporter sont minimes et réalistes.


### Coûts

Comptez 1 € pour louer une heure une machine avec une carte graphique suffisante pour gérer de très nombreux cas de figure.

Si vous avez une application plus complexe, cela pourra nécessiter plus d'une heure de test, mais on voit que le coût n'est pas si important qu'il pourrait empêcher ce test.

Notez aussi qu'OVHcloud offre 200 € de crédit cloud pour tout nouveau projet Public Cloud, ce qui laisse la possibilité d'évaluer toutes leurs machines avec GPU, sans frais.


## Création de notre machine de test

OVHcloud propose de [louer des machines avec cartes graphiques à l'heure](https://www.ovhcloud.com/fr/solutions/nvidia/), dans leur offre Public Cloud.
C'est ce sur quoi nous allons nous baser pour faire notre évaluation : les prix vont de 1 à 3 € par heure.

### 1. Projet Public Cloud

Pour commencer, nous allons devoir créer un projet Public Cloud.

Rendez-vous dans la console manager OVH, dans la rubrique Public Cloud, pour créer un nouveau projet.

![Créez un nouveau projet](project-public-cloud.png)

Indiquez un nom pour le projet, il ne sera utile que pour se repérer plus tard dans la console OVH.

Un second écran vous demandera le moyen de paiement à débiter.
S'agissant d'une offre cloud, la facture arrive à la fin du mois selon ce que vous avez consommé dans le mois.

Si c'est votre premier projet OVHcloud, n'oubliez pas d'indiquer le code promo pour bénéficier des 200 € de crédits.


### 2. Démarrer une nouvelle instance

Une fois le projet créé, toutes les offres cloud sont à portée de clic.

Celle qui nous intéressera ici est « Compute ».
Nous allons « Créer une instance » :

![L'écran d'accueil de votre projet](onboarding.png)

#### Le type d'instance

Nous voulons une machine avec une carte graphique, rendez-vous donc dans l'onglet GPU.

Un large choix de machine s'offre à nous, avec différentes caractéristiques en terme de matériel, quantité de RAM et carte(s) graphique(s).
Le meilleur choix pour évaluer les modèles est d'utiliser les modèles avec des cartes graphiques NVIDIA L4 ou NVIDIA L40S : respectivement L4-90 ou L40S-90.
Ce dernier type d'instance est 2 fois plus cher à l'heure et ne se révèle utile uniquement si vous comptez évaluer les modèles les plus gros comme [FLUX.1-dev](https://huggingface.co/black-forest-labs/FLUX.1-dev).

![Sélectionnons une instance L4-90](select-ia-instance.png)

#### Emplacement géographique

Choisissez ensuite un datacentre dans lequel démarrer l'instance.
Il n'y en a pas forcément dans tous les datacentres, cela n'a de toute façon pas d'importance s'agissant pour le moment d'une simple évaluation.

#### Choix et configuration du système d'exploitation

Vient ensuite la question du système d'exploitation.

Je vous recommande de choisir la dernière version d'Ubuntu pour utiliser [le script cloud-init]({{< relref "cloud-init-to-deploy-localai-in-5-minutes" >}}) qui déploiera tout le nécessaire automatiquement.

![Sélectionnons le système d'exploitation](select-os.png)

Ajoutez une clef SSH, même si vous n'en aurez vraisemblablement pas besoin aujourd'hui :

![Ajouter une clef SSH](select-ssh-key.png)

#### Configuration de l'instance

Viens maintenant la configuration de l'instance.
Nous allons passer un script de post-installation afin que la machine soit configurée comme il faut, sans avoir à s'y connecter :

![On ajoute un script de post-installation](select-config-1.png)

Placez le contenu du [script `cloud-init` que je vous ai partagé dans cet article]({{< relref "cloud-init-to-deploy-localai-in-5-minutes#machine-avec-carte-graphique" >}}), dans le *petit* champ de texte dédié :

![Le script de post-installation ajouté](select-config-2.png)

{{% card color="warning" title="Script post-installation" %}}

**⚠️ N'oubliez pas de changer le nom de domaine dans le script `cloud-init` !**

{{% /card%}}

Le script de post-installation va automatiquement installer le service [LocalAI](https://localai.io/).
C'est un service qui expose une API entièrement compatible avec OpenAI, mais en utilisant des modèles d'IA directement sur la machine.
Aucune donnée n'est envoyée en dehors de la machine.

#### Mode réseau

Dernière question : comment la machine est-elle connectée.

Pour aller au plus simple, on choisit le mode Public, qui attribuera une IP publique à la machine.

![Le mode public permet d'avoir une IPv4 publique](select-network.png)

Ceci permettra de contacter notre service d'IA sans besoin de passer par un autre service, tel un répartisseur de charge.
Dans un montage de production, on souhaitera évidemment passer par un *load-balancer*, mais c'est ici superflu.

#### C'est parti !

Il ne reste plus qu'à choisir la tarification à l'heure, puis validez le formulaire pour lancer la création de l'instance.

![Le mode de facturation à choisir](select-price.png)

Comptez 10 minutes à ce stade avant que la machine ne soit pleinement opérationnelle :

- 1 minute pour qu'OVH vous alloue une machine et la démarre,
- 8 minutes pour exécuter le script cloud-init : configuration et téléchargement des conteneurs puis téléchargement des modèles.

### 3. Configurer le nom de domaine

Pendant que l'instance démarre, il faut configurer notre nom de domaine pour faire pointer notre sous-domaine `ia.example.com.` vers notre machine fraîchement instanciée.

Après avoir validé la création, les robots d'OVH se mettent à travailler et vont notamment allouer une IP publique à notre machine.
L'opération prend quelques secondes, cliquez sur le bouton pour rafraîchir la liste :

![Actualisez la liste des instances pour obtenir l'IP publique](instances-1.png)

Si la création de l'instance se passe correctement, vous devriez avoir une IP qui vous est attribuée :

![La liste des instances avec l'IP publique de notre machine](instances-2.png)

Il faut maintenant créer un nouveau sous-domaine pointant vers cette IP.
Dans le cas d'OVH, cela se passe dans la partie « Web Cloud » :

![La liste des instances avec l'IP publique de notre machine](domain-1.png)

On crée le sous-domaine `ia`, pointant sur l'IP donnée par OVH :

![La liste des instances avec l'IP publique de notre machine](domain-add.png)

{{% card color="info" title="Fixer le TTL" %}}

Le TTL indique la durée durant laquelle l'enregistrement restera dans le cache des serveurs DNS.
Indiquez une valeur plutôt basse, comme 60 ou 120 (c'est des secondes), pour pouvoir changer de la machine sans attendre trop longtemps.

{{% /card%}}

Si votre hébergeur DNS n'est pas OVH (et même si c'est OVH d'ailleurs), vous pouvez utiliser [happyDomain pour modifier facilement et sans crainte votre domaine](https://www.happydomain.org/).


## Évaluer l'IA

Reste maintenant à modifier notre application pour qu'elle utilise notre service d'IA et non plus OpenAI :

```patch
 client = OpenAI(
-    api_key=os.environ.get("OPENAI_API_KEY"),
+    base_url="https://ia.example.com/v1",
 )
```

Eh oui, c'est tout !
L'API de LocalAI étant compatible, tous les services employés fonctionneront avec les modèles locaux.

Ajoutons tout de même que le nom des modèles diffèrent.
Ainsi, si nous avons bien `gpt-4` dans LocalAI, il s'agit en fait d'un alias vers les modèles `phi-2` ou `hermes-2-pro-mistral` selon la configuration matérielle.
L'interface de LocalAI permet [d'installer d'autres modèles](https://models.localai.io/).
Il peut être nécessaire d'en tester plusieurs, car chacun a des avantages et des inconvénients.

Jetez un œil à ceux-ci notamment :

- [flux.1-dev](https://huggingface.co/black-forest-labs/FLUX.1-dev) pour la génération d'image,
- [hermes-3-llama-3.1-8b](https://huggingface.co/NousResearch/Hermes-3-Llama-3.1-8B) pour générer du texte avec une réflexion intéressante.

----

L'évolution rapide des IA génératives offre des opportunités extraordinaires, mais elle pose également des défis, notamment en matière de protection des données.

Adopter une approche plus souveraine, comme celle proposée dans ce tutoriel, permet de tirer parti des avancées technologiques tout en gardant un contrôle total sur les données sensibles, de maîtrise des coûts ou encore de flexibilité. Cela vous permettra non seulement de garantir la confidentialité de vos informations, mais aussi d’adapter les performances de l’IA à vos besoins spécifiques, sans dépendre entièrement de prestataires externes. N'oubliez pas que l'efficacité d'une IA ne repose pas seulement sur ses capacités techniques, mais aussi sur la manière dont elle est intégrée et gérée au sein de votre infrastructure. En prenant en main ces aspects, vous vous assurez de rester compétitif tout en respectant les régulations en vigueur.
