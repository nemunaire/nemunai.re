---
title: "Who am I?"
date: !!timestamp '2017-07-31T00:50:07+02:00'
update: !!timestamp '2021-07-24T00:00:00+01:00'
menu:
  main:
    name: "About"
    weight: 30
---

Hello! `nemunaire` here, aka Pierre-Olivier; I am an engineer (and passionate about [how things work](https://en.wikipedia.org/wiki/Wikipedia)), a teacher (and a pioneer of [gamification](https://en.wikipedia.org/wiki/Gamification) in my classes), a cook (and an advocate of [limiting the ecological footprint in our eating habits](https://en.wikipedia.org/wiki/Vegetarianism)) and I also fight against the hold of [GAFAM](https://en.wikipedia.org/wiki/GAFAM) on our time. Besides, I generally adhere to [stoicism](https://en.wikipedia.org/wiki/Stoicism). I love typography, challenges, chocolate (or vanilla?? the choice is too hard) and so many other things!

{{<icon "fa fa-briefcase about-icon">}}
Currently **entrepreneur** in various businesses. Previously worked as **devops** for [Novaquark](http://novaquark.com), prior to worked as **embedded software engineer** at [Qarnot computing](https://qarnot.com/), then **Chief Information Security Officer** and **senior software architect** at [Qarnot computing](https://qarnot.com/).

{{<icon "fa fa-graduation-cap about-icon">}}
After 5 years in [Epita](http://epita.fr/), in 2014, I am a graduate engineer! I was following the [***System, Network and Security***](https://srs.epita.fr/) classes.
{{<br>}}
During my studies, I was *root* of the [*assistants'*](https://assistants.epita.fr/) laboratory and of the *System, Network and Security* laboratory.

{{<icon "fa fa-terminal about-icon">}}
With my rich creativity, I'm always working on a lot of amazing projects.
I spend most of my free time to improve system support, document and **promote ARM-based computer** (and now RISC-V), whether embedded, server or desktop.
{{<br>}}
Check out my [gitea instance](https://git.nemunai.re) or my [GitHub account](https://github.com/nemunaire).

{{<icon "far fa-thumbs-down about-icon">}}
You won't find me on any social network, because I don't have time to sell my privacy for free (and I fight against most of them).

{{<icon "fa fa-heart about-icon">}}
I'm crazy about any knowledge (mainly focus on sciences, typography, society, companies, faune and flora, ...) and am looking for more freedom and independence.

{{<icon "fa fa-drum about-icon">}}
On my spare time, I also [play drums](https://storage.nemunai.re/scores/_list.html) and [cook](https://food.p0m.fr/).
