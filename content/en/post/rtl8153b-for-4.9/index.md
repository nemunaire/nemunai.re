---
title: RTL8153B support for 4.9 kernel
date: !!timestamp '2019-03-17 00:00:00'
tags:
    - kernel
---

If you buy a *recent* USB to Ethernet adapter, embedding a Realtek chip, you
possibly face, like me, the following error, when connecting it:

```
r8152 4-1.1:1.0 (unnamed net_device) (uninitialized): Unknown version 0x6010
r8152 4-1.1:1.0 (unnamed net_device) (uninitialized): Unknown Device
```

<!--more-->

This error is raised because your Linux r8152 driver is too old and doesn't
support your chip's variant.

In my case, I got the variant `0x6010` which has only been implemented in Linux
4.13, in the [commit
65b82d69...d3e7fb976](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/drivers/net/usb/r8152.c?h=v4.13&id=65b82d696b9e84fda6dd7df61801b57d3e7fb976).

As I run at the time a 4.9 kernel and can't upgrade to a newer release, I
backport the commit to the 4.9 branch. You can find [the patch I made
here](r8152-for-4.9.patch). It only adds support for RTL8153B.
