---
title: "Duplicate window display on 2 screens with X.org"
date: !!timestamp '2023-12-09 11:34:00'
tags:
- teaching
- conference
- xorg
aliases:
- squint
---

As an engineering school teacher and technical lecturer, I'm often asked to give demonstrations, in addition to the classic slide show.
Like any organized lecturer, I can't do without my notes, elapsed time, etc., which are given to me by presenter screen software.
This kind of software requires an extended display, i.e. the computer screen and the video projector display 2 different things.

The disadvantage of demonstrations is that the application window you want to show cannot be displayed on both screens at the same time.
I've tried to solve this problem, in particular to avoid having to do demos while craning my neck, as the window necessarily had to be on the video projector screen, which is really not comfortable.

Today, I'd like to share with you a discovery that has changed the way I manage my presentations and conferences.

<!-- more -->

## Screen cloning problem

For years, I've had to deal with a particularly restrictive problem in my presentations: the need to duplicate a window on several screens.

During my lectures and conferences, I usually use three separate desktops:

- one for the slide deck
- another for a terminal, and
- one for a web browser.

The complexity lay in the fact that I needed to be able to view my notes on my screen, while at the same time having a different view of the presentation screen for the audience.
The traditional solution of a cloned display was therefore insufficient. I had to use an extended display.


## Duplicate terminal window

In order to duplicate the terminal emulator display, I used a multiplexer, `screen`.
With `screen`, several users can join a session, with the `-x` option (multi-display mode).

On the projection screen, I launch 1 terminal, in large characters to be legible from a distance, also with `screen -x` (launch a session in multi-display mode).

On my computer screen, I launch 2 terminals: one with `screen -r -x` (the `-r` option to join an existing session), the other with my notes.
As my terminal is not displayed in large characters, the 2 terminals run side by side, with normal font size.


## Draft solution: video streaming

A first solution I considered was to display a live video capture of my main screen on the projection screen.

Here's my solution, using `ffmpeg` and `mpv`:

```
ffmpeg -f x11grab -framerate 25 -video_size 1920x1080 -vcodec mpeg4 -q 12 -f mpegts - | mpv --cache=no -
```

My starting point was the videoconferencing demonstration: it works pretty well, so why not use the same principle?

The options are probably not optimal and my machine is relatively old (it doesn't have hardware acceleration for encoding video streams), so the display wasn't as fluid or qualitative as I would have liked.


## `squint` !

After scouring the Internet for someone in the same situation as me, I finally found what I was looking for on GitHub, with [the `squint` project](https://github.com/a-ba/squint).

It compiles and installs very easily.

The icing on the cake is that it can be controlled via a menu in the notification area (which is usually on the main screen, which won't be visible to viewers).

![The menu for configuring squint](squint.webp)


## Professional presentations

The discovery of Squint has greatly improved the efficiency and fluidity of my presentations and conferences.

By sharing this find, I hope to help other teachers and professionals facing similar challenges in managing their Xorg workspaces ([perhaps soon also available for Wayland](https://github.com/a-ba/squint/blob/master/TODO#L3)).
