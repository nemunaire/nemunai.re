---
title: "Dupliquer l'affichage d'une fenêtre sur 2 écrans sous X.org"
date: !!timestamp '2023-12-09 11:34:00'
tags:
- teaching
- conference
- xorg
aliases:
- squint
---

En tant qu'enseignant en école d'ingénieur et conférencier technique, je suis souvent amené à faire des démonstrations, en plus de la diffusion d'un classique diaporama.
Comme tout conférencier organisé, je ne peux me passer de mes notes, du temps écoulé, ... qui me sont données par un logiciel d'écran de présentateur.
Ce genre de logiciel nécessite d'avoir un affichage étendu, c'est-à-dire que l'écran de l'ordinateur et le vidéo projecteur affichent 2 choses différentes.

L'inconvénient, lorsqu'il faut faire des démonstrations, c'est que la fenêtre de l'application que l'on souhaite montrer ne peut pas être affichée sur les deux écrans à la fois.
J'ai cherché à résoudre ce problème, notamment pour éviter de devoir faire les démo en me tordant le cou car le fenêtre devait nécessairement être sur l'écran du vidéo-projecteur, ce qui n'est vraiment pas confortable.

Aujourd'hui, je souhaite partager avec vous une découverte qui a changé la donne dans la gestion de mes présentations et conférences.

<!-- more -->

## Problème du clonage de l'écran

Pendant des années, j'ai été confronté à un problème particulièrement contraignant dans mes présentations : la nécessité de dupliquer une fenêtre sur plusieurs écrans.

Lors de mes cours magistraux ou conférences, j'utilise habituellement trois bureaux distincts :
- un pour le jeu de slides,
- un autre pour un terminal, et
- le dernier pour un navigateur web.

La complexité résidait dans le fait que je devais pouvoir visualiser mes notes sur mon écran, tout en ayant une vue différente sur l'écran de présentation pour le public.
La solution traditionnelle d'un affichage cloné s'avérait donc insuffisante. Je devais nécessairement utiliser un affichage étendu.


## Dupliquer la fenêtre de terminal

Afin de dupliquer l'affichage de l'émulateur de terminal, je passe par un multiplexeur, `screen`.
Avec `screen`, plusieurs utilisateurs peuvent rejoindre une session, avec l'option `-x` (multi-display mode).

Sur l'écran de projection, je lance 1 terminal, en gros caractères pour être lisisble de loin, lui aussi avec `screen -x` (on lance une session en mode multi-display).

Sur l'écran de mon ordinateur je lance donc 2 terminaux : l'un avec `screen -r -x` (l'option `-r` en plus pour rejoindre une session existante), l'autre avec mes notes.
Du fait que mon terminal n'est pas affiché en gros caractères, les 2 terminaux passent bien côte-à-côte, avec une taille de caractères normale.


## Ébauche de solution : streaming vidéo

Une première solution que j'ai envisagée était d'afficher sur l'écran de projection une capture vidéo en direct de mon écran principal.

Voici ma solution, utilisant `ffmpeg` et `mpv` :

```
ffmpeg -f x11grab -framerate 25 -video_size 1920x1080 -vcodec mpeg4 -q 12 -f mpegts - | mpv --cache=no -
```

Je partais du principe de la démonstration en visioconférence : ça fonctionne plutôt bien, alors pourquoi pas utiliser le même principe ?

Les options ne sont sans doute pas optimales et ma machine est relativement vieille (elle n'a pas d'accélération matérielle pour encoder de flux vidéo), alors l'affichage n'était pas aussi fluide ou qualitatif que je l'aurais souhaité.


## `squint` !

Après avoir écumé tout l'Internet à la recherche d'une personne dans la même situation que moi, j'ai fini par trouver mon bonheur sur GitHub, avec [le projet `squint`](https://github.com/a-ba/squint).

Il se compile et s'installe très facilement.

Cerise sur le gâteau, il se contrôle via un menu dans la zone de notification (qui est généralement sur l'écran principal, qui ne sera pas visible des spectateurs).

![Le menu pour configurer squint](squint.webp)


## Des présentations pro

La découverte de Squint a considérablement amélioré l'efficacité et la fluidité de mes présentations et conférences.

En partageant cette trouvaille, j'espère aider d'autres enseignants et professionnels confrontés à des défis similaires dans la gestion de leur espace de travail sous Xorg ([peut-être bientôt disponible aussi pour Wayland](https://github.com/a-ba/squint/blob/master/TODO#L3)).
