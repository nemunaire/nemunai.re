---
title: Use Gitolite Access Control In Gitweb
date: !!timestamp '2015-09-24 00:00:00'
update: !!timestamp '2019-04-09 12:45:00'
tags:
    - git
---

Are you using gitolite and gitweb? Two nice and lightweight projects, but perhaps you are tired to manage access control in gitweb?

Here is some simple tricks to use gitolite access list directly into gitweb, automatically.

<!--more-->

As you may know, the configuration file
[`/etc/gitweb.conf`](https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/gitweb.conf.txt#n15)
is in fact no less than a perl script fragment. As it is simply imported by
gitweb, it is possible to include some code.

[Gitolite](https://gitolite.com/) is also a piece of software writen in
Perl. So it is pretty easy to make gitweb relying on gitolite permissions. Here
is what I append to my `/etc/gitweb.conf`:

```perl
# Most of the code comes from https://gitolite.com/gitolite/gitweb.conf.html
# where comments live.

BEGIN {
    $ENV{HOME} = "/var/lib/gitolite";  # Home of the gitolite user in gentoo
    $ENV{GL_BINDIR} = "/usr/libexec/gitolite";
}

use Gitolite::Easy;

$ENV{GL_USER} = $cgi->remote_user || "gitweb";

$export_auth_hook = sub {
    my $repo = shift;

    return unless $repo =~ s/^\Q$projectroot\E\/?(.+)\.git$/$1/;

    return Gitolite::Easy::can_read($repo);
};
```
