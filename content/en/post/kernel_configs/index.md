---
title: Linux Kernel Configurations
date: !!timestamp '2015-04-20 00:00:00'
update: !!timestamp '2019-08-13 14:15:00'
tags:
    - kernel
---

My favorite distribution is [Gentoo], for 7 years now.
It allows me to have all the flexibility I need (the perfect world between stability with only legacy packages or recent ones on a constantly broken system; as in Gentoo, you always have choice) and it teaches me so many things each day.

As I'm used to control everything, here is a list of kernels' configurations I use currently.

<!--more-->

* [Dreamplug]: latest public Grsecurity kernel 4.9 (before, I used precompiled kernels from [Xilka]);
* [Cubieboard 2]: latest mainline kernel, currently 5.2;
* [Odroid-C1]: Linux 3.10 [custom branch](https://github.com/hardkernel/linux.git) for the Amlogic S805 (quad-core ARMv7 Cortex-A5 and Mali450) + upstream patches on 3.10 not merged in the Hardkernel tree;
* [Mirabox]: latest public Grsecurity kernel 4.9;
* [Cubox-i 4x4]: latest mainline kernel, currently 4.16, running OpenGL applications through etnaviv driver;
* [Creator CI20]: Linux 3.18 [custom branch](https://github.com/MIPS/CI20_linux.git) for the Ingenic JZ4780 SoC + [upstream patches](https://github.com/nemunaire/CI20_linux.git) on 3.18 not merged in the imgtec tree;
* [ThinkPad X250]: latest mainline kernel, currently 5.1.
* [Orange Pi PC]: latest mainline kernel, currently 5.2 on headless server.
* [VoltaStream AMP1] & [VoltaStream Zero]: Linux 4.9 [custom branch](https://github.com/polyvection/linux-imx.git) for [i.MX6ULL](http://git.freescale.com/git/cgit.cgi/imx/linux-imx.git) + [upstream patches](https://github.com/nemunaire/linux-imx.git) on 4.9 not merged in the PolyVection tree.
* [Pine A64]: latest mainline kernel, currently 5.0 on headless server.
* [Helios4]: latest mainline kernel, currently 5.2 on headless server.

[Gentoo]: http://www.gentoo.org/
[Dreamplug]: http://www.globalscaletechnologies.com/p-54-dreamplug-devkit.aspx
[Xilka]: http://www.xilka.com/sheeva/
[Odroid-C1]: http://www.hardkernel.com/main/products/prdt_info.php?g_code=G141578608433
[Cubieboard 2]: http://cubieboard.org/model/cb2/
[Mirabox]: http://www.globalscaletechnologies/p-58-mirabox-java-devkit.aspx
[Cubox-i 4x4]: http://www.solid-run.com/product/cubox-i-4x4
[Creator CI20]: http://store.imgtec.com/uk/product/mips-creator-ci20/
[ThinkPad X250]: https://wiki.gentoo.org/wiki/Lenovo_Thinkpad_X250
[Orange Pi PC]: http://www.orangepi.org/orangepipc/
[VoltaStream AMP1]: https://web.archive.org/web/20190323152013/https://voltastream.com/product/voltastream-amp1/
[VoltaStream Zero]: https://web.archive.org/web/20190323152013/https://voltastream.com/product/voltastream-zero/
[Pine A64]: https://www.pine64.org/devices/single-board-computers/pine-a64/
[Helios4]: https://kobol.io/
