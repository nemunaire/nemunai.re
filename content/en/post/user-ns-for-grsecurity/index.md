---
title: Support for the user namespace in grsecurity kernel
date: !!timestamp '2018-10-31 00:00:00'
tags:
    - kernel
---

Grsecurity has completely disabled, [on
purpose](https://forums.grsecurity.net/viewtopic.php?f=3&t=3929#p13904), the
user namespace code for the kernel.

As the goal of this namespace is to gain (virtualy) root privilegies inside a
namespace (in theory, it shouldn't give more priviledgies than the one you
initialy have outside of your namespace), there are some interesting use cases,
or, in my case I need to perform some demo in front of my students.

<!--more-->

Other distros, like Debian, choose to allow the use of user namespace through a
custom kernel config variable. In order to use them without both
`CAP_SYS_ADMIN`, `CAP_SETUID` and `CAP_SETGID`, you need to activate (as root):

```bash
42sh# sysctl -w kernel.unprivileged_userns_clone=1
```

To reproduce the same behaviour with a grsecurity kernel, I made a patch, to
apply after the grsecurity patch:
[grsec-enable-user-ns.patch](grsec-enable-user-ns.patch).
