---
title: "Qui suis-je ?"
date: !!timestamp '2017-07-31T00:50:07+02:00'
update: !!timestamp '2021-07-24T00:00:00+01:00'
menu:
  main:
    name: "À propos"
    weight: 30
aliases:
  - about
---

Bonjour ! ici `nemunaire`, alias Pierre-Olivier ; je suis ingénieur (et passionné par  [le fonctionnement des choses](https://fr.wikipedia.org/wiki/Wikipédia)), enseignant (et pionnier de la [gamification](https://fr.wikipedia.org/wiki/Ludification) de mes cours, [voici un exemple]({{< relref "post/gamification-of-a-advanced-linux-sysadmin-course/_index.md" >}})), cuisinier (et partisan de [limiter l'empreinte écologique dans nos habitudes alimentaires](https://fr.wikipedia.org/wiki/V%C3%A9g%C3%A9tarisme)) et je lutte aussi contre l'emprise des [GAFAM](https://fr.wikipedia.org/wiki/GAFAM) sur notre temps. D'ailleurs j’adhère de manière générale au [stoïcisme](https://fr.wikipedia.org/wiki/Sto%C3%AFcisme). J'adore la typographie, les challenges, le chocolat (ou la vanille ?? le choix est trop difficile) et tant d'autres choses !

{{<icon "fa fa-briefcase about-icon">}}
Aujourd'hui **entrepreneur**. J'ai travaillé avant comme **devops** pour [Novaquark](http://novaquark.com), avant d'être **ingénieur système/logiciel embarqué** chez [Qarnot computing](https://qarnot.com/), puis **responsable de la sécurité des systèmes d'information** et **architecte logiciel sénior** chez [Qarnot computing](https://qarnot.com/).

{{<icon "fa fa-graduation-cap about-icon">}}
Après 5 ans d'études à l'[Epita](http://epita.fr/), j'ai, en 2014, obtenu mon diplôme d'ingénieur ! J'ai suivi les enseignements de la majeure [***Systèmes, Réseaux et Sécurité***](https://srs.epita.fr/).
{{<br>}}
Durant mes études, j'étais *root* (responsable du parc informatique) du [laboratoire des assistants](https://assistants.epita.fr/) ainsi que du laboratoire *Systèmes, Réseaux et Sécurité*.

{{<icon "fa fa-terminal about-icon">}}
L'esprit toujours en ébulition, je travaille constamment sur de nombreux projets passionnants.
Je passe aussi beaucoup de temps à contribuer à des projets libres : généralement à améliorer le support, la documentation et faire la promotion des **ordinateurs à base de processeurs ARM**, et maintenant RISC-V.
{{<br>}}
Jetez un œil à mon [instance gitea](https://git.nemunai.re) ou à mon [compte GitHub](https://github.com/nemunaire).

{{<icon "far fa-thumbs-down about-icon">}}
Vous ne me trouverez pas sur les réseaux sociaux : je n'apprécie pas de gaspiller mon temps pour vendre ma vie privée gratuitement (d'ailleurs je lutte activement contre leur usage).

{{<icon "fa fa-heart about-icon">}}
Découvrir de nouvelles connaissances et techniques est quelque chose que j'apprécie particulièrement (surtout dans les domaines des sciences, de la typographie, des entreprises, de la faune et de la flore, ...).
Je recherche plus de libertés au sens large et d'indépendance.

{{<icon "fa fa-drum about-icon">}}
Sur mon temps libre, je joue de [la batterie](https://storage.nemunai.re/scores/_list.html) et je [cuisine](https://food.p0m.fr/).
