---
title: Utiliser les plages IPv6 supplémentaires du réseau Free et Orange
date: !!timestamp '2023-04-05 14:43:00'
tags:
- network
- ipv6
- freebox
---

Chez Free et Orange, lorsque l'on n'a pas désactivé l'IPv6 les Freebox (et certaines Livebox) mettent à disposition des équipements connectés une plage d'IPv6 /64.
Mais il se trouve en fait que c'est une plage /60 qui est mise à disposition et utilisable par chaque abonné.
Cela représente en tout 8 réseaux /64 adressables.
Voyons à quoi cela peut-il bien servir et comment les utiliser.

<!-- more -->

# Rappels IPv6

Contrairement à l'IPv4, avec des IPv6 on évite de faire du NAT, c'est-à-dire que l'on attribue à chaque machine sur le réseau une adresse IPv6 directement routable sur Internet.
Bien entendu il est toujours nécessaire de passer par le routeur (la box) qui sert alors de simple passerelle vers Internet.

En IPv6, les équipements sont capables de choisir eux-même leur IP, sans l'aide du protocole DHCP.
Cela est possible car le routeur émet régulièrement les informations du sous-réseau dans lequel on se trouve (il s'agit du [Router Advertisement (RA)](https://en.wikipedia.org/wiki/Router_advertisement)).

Pour notre expérience, prenons le lab suivant :

![L´infrastructure de base qui va nous servir à faire nos expérimentations](lab.png)

Nous avons tous nos équipements reliés à la box et une série de machines virtuelles hébergées sur l'une des machines du réseau.

À ce stade, si l'on veut que nos machines virtuelles soient joignables depuis Internet en IPv6, on est obligé de configurer le réseau de l'hyperviseur en mode *bridge*.\
En effet si le réseau de nos machines virtuelles est distinct du réseau de la box, celle-ci ne sera pas en mesure de communiquer avec nos machines virtuelles. En utilisant le mode *bridge*, on simule le fait que la machine virtuelle est directement reliée à la box, ou à un switch. En tous cas aucun équipement nécessitant de faire du routage.

Si nos machines virtuelles sont uniquement des clients IPv6 et n'ont pas vocation à servir directement du contenu sur Internet, cette solution est tout-à-fait acceptable. Mais si on veut servir du contenu, on pourrait vouloir segmenter notre réseau pour tenter d'isoler les équipements.


# Segmenter le réseau de la box

Du fait du très grand nombre d'adresses IPv6 publiques que nos opérateurs nous fournissent, on pourrait commencer par vouloir segmenter notre réseau entre nos serveurs virtuels et nos autres équipements : chacun serait dans un sous-réseau séparé.

L'intérêt principal de cette segmentation serait d'éviter que tout ce petit monde partage le même sous-réseau : comme ils peuvent tous communiquer directement entre-eux, il est plus compliqué de filtrer efficacement les échanges malveillant. Par exemple si l'une des machines virtuelles exposée sur Internet est compromise, elle peut accéder à tous nos équipements locaux (téléphones, objects connectés, ...) qui ne sont pas forcément sécurisés, ou inversement, un objet du réseau peut se mettre à intercepter tout le trafic des machines virtuelles en se faisant passer pour la box.

On pourrait donc vouloir segmenter son réseau comme cela :

![Exemple de segmentation par le partage du bloc /64 en deux blocs /65](lab-segmente.png)

On réserverait la moitié du bloc /64 aux équipements réels du réseau et on attribuerait l'autre moitié à nos machines virtuelles situées sur un serveur/Raspberry Pi.

Malgré le très grand nombre d'adresses IPv6 que l'on peut attribuer, il n'est pas évident de subdiviser notre /64 pour l'attribuer à un routeur secondaire ou un serveur de machines virtuelles. Cette segmentation n'est en effet pas possible sans changer la configuration de la box car celle-ci s'attend à pouvoir joindre nos machines virtuelles directement, sans passer par une machine/hôte/routeur intermédiaire.

Cependant nous avons accès aux paramètres de routage des 7 autres blocs /64 distribués par l'opérateur. Nous pouvons par exemple en attribuer un à l'hôte de nos machines virtuelles.


# Déléguer un préfixe IPv6 supplémentaire

Comme on le disait en introduction, certains opérateurs mettent à disposition de leur abonnés une plage d'adresses IPv6 bien plus grande que le bloc /64 du réseau principal.

Certaines box permettent en outre de tirer parti des blocs complémentaires en proposant de déléguer les autres blocs à des machines du réseau.

Concrètement, cela signifie que lorsque la box recevra un paquet à destination d'un des blocs délégués, elle ne le traitera pas elle-même, elle le transmettra à la machine désignée comme destinataire. En d'autres termes, elle routera le trafic de ce bloc vers le routeur désigné. Et ce n'est pas forcément compliqué !


# Différents cas d'usage

Maintenant que l'on a vu la théorie, voyons justement différents cas d'usage, pour ne pas nous limiter à nos machines virtuelles :

- Utiliser un bloc /64 pour donner de l'IPv6 à ses machines virtuelles
- [Utiliser un bloc /64 pour donner de l'IPv6 à ses conteneurs Docker]({{< relref "use-ipv6-in-docker.md" >}})
- Utiliser un bloc /64 pour avoir de l'IPv6 dans plusieurs sous-réseaux isolés
- Utiliser un bloc /64 pour avoir de l'IPv6 publique dans son tunnel Wireguard
